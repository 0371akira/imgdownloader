# ImgDownloaderの使い方

---
```
cd e:
python -m venv venv_imgDownloader
```
Linux，Mac
```
source venv_imgDownloader/bin/activate
```
Windows
```buildoutcfg
.\venv_imgDownloader\Scripts\activate
```
---
```buildoutcfg
git clone git@bitbucket.org:0371akira/imgdownloader.git
pip install -r requirements.txt
django-admin startproject imgDownloader .
python3 manage.py startapp imgDownloaderApp
python manage.py makemigrations --settings=imgDownloader.settings
python manage.py migrate --settings=imgDownloader.settings
python manage.py runserver --settings=imgDownloader.settings
```