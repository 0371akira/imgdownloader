from django.apps import AppConfig


class ImgdownloaderappConfig(AppConfig):
    name = 'imgDownloaderApp'
