from django.shortcuts import render
from .models import ImgURL
from django.views.generic import CreateView
from django.urls import reverse_lazy
from pathlib import Path
import urllib.request
import requests
import urllib
import time
from bs4 import BeautifulSoup


class Create(CreateView):
   template_name = 'home.html'
   model = ImgURL
   fields = ('url',)
   success_url = reverse_lazy('clickButton')


def clickButtonfunc(request):
   url_list = []
   for post in ImgURL.objects.all():
      load_url = post.url
   r = requests.get(load_url)
   # BeautifulSoupでURLをHTMLの解析器（parser）にかける）
   soup = BeautifulSoup(r.content, "html.parser")

   # ダウンロード用のディレクトリを作成
   out_folder = Path("download")
   out_folder.mkdir(exist_ok=True)

   # URLの中にあるすべてのimgタグを対象にする
   for element in soup.find_all("img"):
      # imgタグのsrc属性を取得
      src = element.get("src")
      print("src [" + src + "]")

      # imgタグの画像リンクを絶対パスで取得
      image_url = urllib.parse.urljoin(load_url, src)
      print("image_url [" + image_url + "]")

      # 正常にアクセスできるか確認
      imgdata = requests.get(image_url)
      print("imgdata [", imgdata, "]")

      # ファイル名を取得（[-1]はsplitで分割した文字列のリストの末尾を指定している）
      filename = image_url.split("/")[-1]
      print("filename [", filename, "]")

      # ダウンロード用のディレクトリと画像名をつなげる
      out_path = out_folder.joinpath(filename)
      print("out_path [", out_path, "]")
      print("---")

      # 画像データをファイルに書き出す
      with open(out_path, mode="wb") as f:
         f.write(imgdata.content)

      # リストに表示させる
      url_list.append([filename, image_url])
      # アクセスしたら1秒待つ
      time.sleep(1)

   context = {'url_list': url_list,}
   return render(request, 'list.html', context)