from django.urls import  path
from .views import Create, clickButtonfunc

urlpatterns = [
    path('', Create.as_view(), name='home'),
    path('clickButton/', clickButtonfunc, name='clickButton'),
]